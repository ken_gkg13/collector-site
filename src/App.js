import React, { Component } from "react"
import userLogin from './userportal/UserLogin.js';
import userDashboard from './userportal/UserDashboard.js';
import "./App.css"
import { BrowserRouter as Router,Route,Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
     
      <Router>
        <div className="App">
        <Switch>
          <Route path="/login" exact component={userLogin}/>
          <Route path="/dashboard" exact component={userDashboard}/>
          
        </Switch>
        </div>
      </Router>
    )
  }
}

export default App
