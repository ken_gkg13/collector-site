
import React, { Component } from 'react';
import abstractVector from '../images/hexagon.png';


export default class UserLogin extends Component {
   
   
    login = async e => {
        e.preventDefault();
         await this.mainLogin();
    };

    async mainLogin(){
        //Add Validations
        window.location = "/dashboard";
    }

    render() {
        return (<div>
            <div className="content-full background-cover bg-orange-gradient">
                <div className="row div-center-full">
                    <div className="form-group form-inline col-md-10 col-md-offset-1 bg-white border-rounded-md container-height-md">
        
                        <div className="col-md-6 space-top-sm login-img">
                            <br />
        
                            <h1 className="section-header animate fadeInLeft animated">Collector site v.0.00001</h1>
                            <div className="col-md-10 col-md-offset-1">
                                <img src={abstractVector} alt="" class="img-responsive animated fadeIn login-img" style={{maxWidth: "300px;"}}/>
                                <br/>
                                <br/>
                                <hr/>
                                <p className="text-dim align-justify font-sm">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                </p>
                                <br />
                                <br />
                            </div>
        
                        </div>
        
                        <div className="col-md-6 login-container">
                            <div className="col-md-12 pad-s">
                                <h3 style={{ color: "#2c3e50" }}><i className="fa fa-user" style={{ marginRight: "8px" }}></i>MEMBER LOGIN</h3>
                                <div className="row">
                                    <div className="form-group form-inline col-md-12">
                                        <label id="lblERROR" className="lbl-signup  col-md-12">
                                        </label>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group form-inline col-md-12">
        
                                        <input id="txtUSERNAME" ref={this.textUser} type="text" className="form-control col-md-12 form-signup" placeholder="Email" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group form-inline col-md-12">
                                        <input id="txtPASSWORD" type="password" className="form-control col-md-12 form-signup" placeholder="Password" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group form-inline col-md-12">
                                        <input id="btnLOGIN" type="button" value="Login" className="btn btn-primary " style={{width: "100%" }} onClick={this.login}/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group form-inline col-md-12">
                                        <a href="#!" className="lbl-signup  col-md-12">Forgot your Password?</a>
                                    </div>
                                </div>
        
                                <div className="row">
                                    <div className="form-group form-inline col-md-12">
                                        <br/>
                                        <br/>
                                        <hr/>
                                        <h4 className="lbl-signup col-md-12">Dont have an account?</h4>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group form-inline col-md-12">
                                        <input id="btnSINGUP" type="button" value="Signup" className="btn btn-default " style={{width: "100%" }} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group form-inline col-md-12">
                                        <p className="text-dim align-justify font-sm">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>
        
                </div>
            </div>
        </div>
        );
    }

}
