import React, { Component } from 'react';
import UserNavbar from './UserNavbar.js';


export default class UserDashboard extends Component {


    render() {
        return (<div>

       <UserNavbar />
       <div className="main-content background-cover content-full  bg-darkblue-gradient pad-zero"  style={{marginTop: 44}}>
                <div className="container-full bg-white2 col-md-10 col-md-offset-1">
                    <div className="col-md-12 pad-zero">
                        <div className="col-md-2 side-menu">
                            <ul className="sidebar-nav pad-zero">
                                <li className="sidebar-li"><a href="#!"><i className="fa fa-newspaper"></i>Feed</a></li>
                                <li className="sidebar-li"><a href="#!"><i className="fa fa-image"></i>My Collections</a></li>
                                <li className="sidebar-li"><a href="#!"><i className="fa fa-users"></i>Groups</a></li>
                                <li className="sidebar-li"><a href="#!"><i className="fa fa-shopping-bag"></i>Marketplace</a></li>
                            </ul>
                        </div>
                        <div className="col-md-7 side-menu">
                            <div className="col-md-12  box-panel-sm">
                                <textarea className="txt-share" placeholder="What's going on?">

                                </textarea>
                            </div>
                            <div className="col-md-12  box-panel">
                            </div>
                            <div className="col-md-12  box-panel">
                            </div>


                        </div>
                        <div className="col-md-3 side-menu">
                            <div className="col-md-12  box-panel">
                                <h4><i className="fa fa-star"></i>Popular</h4>
                            </div>
                            <div className="col-md-12  box-panel-md">
                                <h4><i className="fa fa-users"></i>People you may know</h4>
                            </div>
                            <div className="col-md-12  box-panel">
                                <h4><i className="fa fa-info-circle"></i>You might be interested in</h4>
                            </div>
                        </div>


                    </div>
                </div>
        </div>


        </div>);
    }

}
