
import React from 'react'
import abstractVector from '../images/abstract.png';
function UserNavbar() {
    return (
        <nav className="navbar navbar-inverse navbar-fixed-top navbar-portal" role="navigation">
        <div className="container-header">
            <div className="navbar-header background-header">
                <div className="navbar-form navbar-left logo-left">
                    <div className="bb-icon col-md-1 pad-zero">
                        <img src={abstractVector} className="portal-logo" alt=""/>
                    </div>
                    <div className="bb-label col-md-10 pad-zero">
                    </div>

                </div>
            </div>
            {/*[Top Menu Items]*/}
            <div className="background-header">
                <ul className="nav navbar-nav navbar-left text-center nav-left-admin">
                    {/*
                    <li><a href="./home">File</a></li>
                    <li><a href="./category?tag=music">Print</a></li>
                    <li><a href="./category?tag=anime">MUSIC</a></li>
                    */}
                    <li className="li-navportal-search"><input type="text" className="form-control" placeholder="Search..."></input></li>
                </ul>
                <ul className="nav navbar-nav navbar-right text-center">
                    <li><a href="https://www.jobstreet.com.ph/"><i className="fa fa-user fa-fw"></i>Welcome, User</a></li>
                </ul>
            </div>

        </div>
    </nav>
    );
}

export default UserNavbar;
