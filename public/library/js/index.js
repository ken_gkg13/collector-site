$(window).load(function() {
    //document.getElementById("intro").pause();
    loadAnimation();
    initializeCollapse();
    initializeTooltips();
    //disableInspect();
    snowEffect();
    slick();
    sidebar();


        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            orientation: 'top left',
            autoclose: true
        });
    });



    function loadAnimation(){
        $(".trendbox").css("display", "none");
        setTimeout(function() {
            //document.getElementById("intro").play();
            hideProgressBar();
        }, 100);
    }

    function initializeCollapse(){
                $('.collapse').on('show.bs.collapse', function() {
            $('.in').each(function() {
                $(this).collapse('hide');
            });
        });

    }

    function hideProgressBar() {

        $(".trendbox").css("display", "");
        $('.progressBackgroundInitialize').attr("style", "display:none");
        //window.scrollTo(document.body.scrollHeight,0);
    }

    function initializeTooltips(){
         $('[data-toggle="tooltip"]').tooltip();
    }



    function disableInspect(){
             document.addEventListener('contextmenu', function(e) {
          e.preventDefault();
        });

        document.onkeydown = function(e) {
          if(event.keyCode == 123) {
             return false;
          }
          if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
             return false;
          }
          if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
             return false;
          }
          if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
             return false;
          }
          if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
             return false;
          }
        }
    }



    function snowEffect(){

         (function() {
        var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame ||
            function(callback) {
                window.setTimeout(callback, 1000 / 60);
            };
        window.requestAnimationFrame = requestAnimationFrame;
    })();

    var flakes = [],
        canvas = document.getElementById("canvas"),
        ctx = canvas.getContext("2d"),
        flakeCount = 400,
        mX = -100,
        mY = -100

    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    function snow() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        for (var i = 0; i < flakeCount; i++) {
            var flake = flakes[i],
                x = mX,
                y = mY,
                minDist = 150,
                x2 = flake.x,
                y2 = flake.y;

            var dist = Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y)),
                dx = x2 - x,
                dy = y2 - y;

            if (dist < minDist) {
                var force = minDist / (dist * dist),
                    xcomp = (x - x2) / dist,
                    ycomp = (y - y2) / dist,
                    deltaV = force / 2;

                flake.velX -= deltaV * xcomp;
                flake.velY -= deltaV * ycomp;

            } else {
                flake.velX *= .98;
                if (flake.velY <= flake.speed) {
                    flake.velY = flake.speed
                }
                flake.velX += Math.cos(flake.step += .05) * flake.stepSize;
            }

            ctx.fillStyle = "rgba(255,255,255," + flake.opacity + ")";
            flake.y += flake.velY;
            flake.x += flake.velX;

            if (flake.y >= canvas.height || flake.y <= 0) {
                reset(flake);
            }

            if (flake.x >= canvas.width || flake.x <= 0) {
                reset(flake);
            }

            ctx.beginPath();
            ctx.arc(flake.x, flake.y, flake.size, 0, Math.PI * 2);
            ctx.fill();
        }
        requestAnimationFrame(snow);
    };

    function reset(flake) {
        flake.x = Math.floor(Math.random() * canvas.width);
        flake.y = 0;
        flake.size = (Math.random() * 3) + 2;
        flake.speed = (Math.random() * 1) + 0.5;
        flake.velY = flake.speed;
        flake.velX = 0;
        flake.opacity = (Math.random() * 0.5) + 0.3;
    }

    function init() {
        for (var i = 0; i < flakeCount; i++) {
            var x = Math.floor(Math.random() * canvas.width),
                y = Math.floor(Math.random() * canvas.height),
                size = (Math.random() * 3) + 2,
                speed = (Math.random() * 1) + 0.5,
                opacity = (Math.random() * 0.5) + 0.5;

            flakes.push({
                speed: speed,
                velY: speed,
                velX: 0,
                x: x,
                y: y,
                size: size,
                stepSize: (Math.random()) / 30,
                step: 0,
                opacity: opacity
            });
        }

        snow();
    };

    window.addEventListener("mousemove", function(e) {
        mX = e.clientX,
            mY = e.clientY
    });

    window.addEventListener("resize", function() {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

    })

    init();
    }

    function slick() {
        $('.slider-main').slick({
          dots: true,
          infinite: true,
          slidesToShow: 1,
          autoplay: true,
          fade: true,
          cssEase: 'linear',
          speed: 1000,
          autoplaySpeed: 1500
    
        });
      }

$('#btn-search').click(function () {
    var search_key = $('#txt-search').val();
    window.location.href = "browse.html?q=" + search_key;
});


$("#txt-search").bind('keyup', function (e) {
    if (e.keyCode == 13) {
        $('#btn-search').click();
    }
    
});



function sidebar() {

    var $pagebox = $('#page-content');
    var $slidebox = $('#sidebar');

    $(window).resize(function () {
        if ($(window).width() >= 780) {
            $('.sidebar-icon').attr('style', 'display:none;');
            $('.sidebar-link').attr('style', ' font-size: 13px;');
            $slidebox.animate({ left: 0 }, 300);
            $pagebox.addClass('with-sidebar');
            $pagebox.removeClass('full-width');

        } else {
            $('.sidebar-icon').attr('style', 'display:inline-block;');
            $('.sidebar-link').attr('style', ' font-size: 8px;');
            $slidebox.animate({ left: -140 }, 300);
            $pagebox.removeClass('with-sidebar');
            $pagebox.addClass('full-width');
        }
    });
    $('.switcher').click(function (e) {

        e.preventDefault();
        if ($slidebox.css('left') == "0px") {

            $('.sidebar-icon').attr('style', 'display:inline-block;');
            $('.sidebar-link').attr('style', ' font-size: 8px;');
            $pagebox.removeClass('with-sidebar');
            $pagebox.addClass('full-width');
            $.cookie('sidebar', 'full-width', { expires: 300 });
            $slidebox.animate({ left: -140 }, 300);
            $pagebox.animate({ fade: 220 }, 300);
            $pagebox.animate({ left: -10 }, 300);
        }
        else {
            $('.sidebar-icon').attr('style', 'display:none;');
            $('.sidebar-link').attr('style', ' font-size: 13px;');

            $pagebox.removeClass('full-width');
            $pagebox.addClass('with-sidebar');
            $.cookie('sidebar', null, { expires: 300 });
            $slidebox.animate({ left: 0 }, 300);
            $pagebox.animate({ fade: 220 }, 300);
            $pagebox.animate({ left: 0 }, 300);

        }
    });
    if ($.cookie('sidebar') == 'full-width' && $(window).width() >= 768) {
        $('.sidebar-icon').attr('style', 'display:block;');
        $('.sidebar-link').attr('style', ' font-size: 8px;')
        $pagebox.addClass('full-width');
        $slidebox.animate({ left: -140 }, 1);
    }
    else {
        $('.sidebar-icon').attr('style', 'display:none;');
        $('.sidebar-link').attr('style', ' font-size: 13px;');
        $slidebox.animate({ left: 0 }, 1);

        $pagebox.addClass('with-sidebar');
    }

    // By click



};